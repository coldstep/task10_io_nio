package AllTest;

import static com.task10_IO_NIO.part6.SomeBuffer.readSome;
import static com.task10_IO_NIO.part6.SomeBuffer.writeSome;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

class Test6 {

  @Test
  void testChannel() {
    String path = "src/main/resources/files/sometext.txt";
    writeSome(Paths.get(path), "Hello world!");
    assertTrue(new File(path).exists());
    readSome(Paths.get("src/main/resources/files/sometext.txt"), 128);
  }
}
