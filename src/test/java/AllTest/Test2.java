package AllTest;

import static com.task10_IO_NIO.part2.Converter.testWithBuffer;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.Test;
class Test2 {

  @Test
  void testConverter() throws IOException {
// too long
//    testWithoutBuffer();
    for (int i=1 ; i<=10; i++){
      testWithBuffer(i);
    }
    assertTrue(new File("src/main/resources/files/testfile2.txt").exists());
  }
}
