package AllTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.task10_IO_NIO.part1.Droid;
import com.task10_IO_NIO.part1.Ship;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class Test1 {

  private static Droid droid1;
  private static Droid droid2;
  private static Droid droid3;
  private static Droid droid4;
  private static Ship ship;

  @BeforeAll
  static void init() {
    droid1 = new Droid("Bumblebee", 700, 200, 500);
    droid2 = new Droid("Optimus Prime", 1500, 300, 1500);
    droid3 = new Droid("Ratchet", 1000, 100, 1000);
    droid4 = new Droid("Jazz", 500, 250, 500);
    ship = new Ship("Colosus");
    ship.addDroids(droid1, droid2, droid3, droid4);
  }

  @Test
  void testSerialization() throws IOException {
    FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/objects/ship.ser");
    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
    objectOutputStream.writeObject(ship);
    objectOutputStream.close();
    fileOutputStream.close();
    File file = new File("src/main/resources/objects/ship.ser");
    assertTrue(file.exists());
  }

  @Test
  void testDeserialization() {
    try (ObjectInputStream objectInputStream = new ObjectInputStream(
        new FileInputStream("src/main/resources/objects/ship.ser"))) {

      ship = (Ship) objectInputStream.readObject();

    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      assertNotNull(ship);
      ship.showPassengers();
    }
  }



}
