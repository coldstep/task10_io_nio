package com.task10_IO_NIO.part4;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Paths;


public class CustomFilter {

  public static void getComment(String fileName) throws IOException {
    Files.lines(Paths.get(fileName)).filter(s -> s.contains("*")).forEach(System.out::println);
  }
}
