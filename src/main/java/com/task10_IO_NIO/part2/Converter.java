package com.task10_IO_NIO.part2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Random;

public class Converter {

  public static void testWithoutBuffer() throws IOException {
    System.out.println(new Date());
    DataInputStream dataInputStream = new DataInputStream(
        new FileInputStream(
            "src/main/resources/files/testfile1.txt")
    );
    DataOutputStream dataOutputStream = new DataOutputStream(
        new FileOutputStream("src/main/resources/files/testfile2.txt"));
    int data = dataInputStream.read();
    while (data != -1) {
      dataOutputStream.write(data);
      data = dataInputStream.read();
    }

    System.out.println(new Date());
  }

  public static void testWithBuffer(int bufferSize) throws IOException {
    System.out.println("BufferSize - " + bufferSize);
    System.out.println(new Date());

    DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(
        new FileInputStream(
            "src/main/resources/files/testfile1.txt"),
        bufferSize * 1084));
    DataOutputStream dataOutputStream = new DataOutputStream(
        new BufferedOutputStream(new FileOutputStream("src/main/resources/files/testfile2.txt")));
    int data = dataInputStream.read();
    while (data != -1) {
      dataOutputStream.write(data + new Random().nextInt());
      data = dataInputStream.read();
    }
    System.out.println(new Date());
  }
}
