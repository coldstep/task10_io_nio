package com.task10_IO_NIO.part1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Ship implements Serializable {

  private String shipName;
  private List<Droid> ship;
  private transient long shipSize;

  public Ship() {
    ship = new ArrayList<>();
  }

  public Ship(String shipName) {
    this.shipName = shipName;
    ship = new ArrayList<>();
  }

  public String getShipName() {
    return shipName;
  }

  public void setShipName(String shipName) {
    this.shipName = shipName;
  }

  public void addDroids(Droid... droids) {
    if (droids.length >= 2) {
      ship.addAll(Arrays.asList(droids));
    } else {
      ship.remove(droids[0]);
    }
    shipSize = ship.size();
  }

  public void throwOutDroids(Droid... droids) {
    if (droids.length >= 2) {
      ship.removeAll(Arrays.asList(droids));
    } else {
      ship.remove(droids[0]);
    }
    shipSize = ship.size();
  }

  public List<Droid> getShip() {
    return ship;
  }

  public long getShipSize() {
    return shipSize;
  }

  public void showPassengers() {
    System.out.printf("Ship:%25s\n",shipName);
    ship.forEach(System.out::println);
  }

  @Override
  public String toString() {
    return "Ship{" +
        "shipName='" + shipName + '\'' +
        ", ship=" + ship +
        ", shipSize=" + shipSize +
        '}';
  }
}
