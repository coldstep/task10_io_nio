/*
 * Copyright (c)  Systems, Inc.
 * This software is first part of home task
 */
package com.task10_IO_NIO.part1;

import java.io.Serializable;

/**
 * This class was created for implement first part of homework.
 *
 * @author Volodia Bakuro
 * @version 1.0
 * @since 2019-01-01
 */
public class Droid implements Serializable {

  /**
   * Droid name.
   */
  private String name;

  /**
   * Droid health.
   */
  private int health;

  /**
   * Droid damage.
   */
  private int damage;

  /**
   * Droid armor.
   */
  private int armor;

  /**
   * Variable for test serialization.
   */
  private transient int year = 2018;
  /**
   * Simple constructor.
   */
  public Droid() {
  }
  /**
   * Constructor for setting.
   */
  public Droid(String name, int health, int damage, int armor) {
    this.name = name;
    this.health = health;
    this.damage = damage;
    this.armor = armor;
  }
  /**
   * Name getter.
   */
  public String getName() {
    return name;
  }
  /**
   * Name setter.
   */
  public void setName(String name) {
    this.name = name;
  }
  /**
   * Health getter.
   */
  public int getHealth() {
    return health;
  }
  /**
   * Health setter.
   */
  public void setHealth(int health) {
    this.health = health;
  }
  /**
   * Damage getter.
   */
  public int getDamage() {
    return damage;
  }
  /**
   * Damage setter.
   */
  public void setDamage(int damage) {
    this.damage = damage;
  }
  /**
   * Armor getter.
   */
  public int getArmor() {
    return armor;
  }
  /**
   * Armor setter.
   */
  public void setArmor(int armor) {
    this.armor = armor;
  }

  /**
   * Method for droid attack.
   */
  public void attack() {
    System.out.println("Droid attack !");
  }
  /**
   * Method for droid defense.
   */
  public void defense() {
    System.out.println("Droid are defends !");
  }
  /**
   * Overiden method toString().
   */
  @Override
  public String toString() {
    return "Droid{" +
        "name='" + name + '\'' +
        ", health=" + health +
        ", damage=" + damage +
        ", armor=" + armor +
        ", year=" + year +
        '}';
  }
}
