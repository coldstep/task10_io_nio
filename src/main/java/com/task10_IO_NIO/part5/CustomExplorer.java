package com.task10_IO_NIO.part5;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class CustomExplorer {

  private File file;
  private String lastPath;


  public static void main(String[] args) {
    new CustomExplorer().init();
  }

  public void init() {
    System.out.println(
        "Welcome \n You can use default command 'cd' or 'dir' or 'exit' to get info about directories or files");
    Scanner scanner = new Scanner(System.in);
    lastPath = "C:/";
    cd(lastPath);
    dir();
    while (true) {
      System.out.println("\nPlease write default command to get info:");
      System.out.printf(lastPath + ">");
      analyse(scanner.nextLine());
    }

  }

  private void analyse(String path) {
    if (path.contains("cd ")) {
      path = path.split("cd ")[1];
      if (!path.contains("c:/")) {
        if (lastPath.endsWith("/")) {
          path = lastPath + path;
        } else {
          path = lastPath + "/" + path;
        }
      }
      cd(path);
    } else if (path.contains("dir")) {
      dir();
    } else if (path.equals("exit")) {
      System.exit(0);
    } else {
      System.out.println("wrong command !");
    }
  }

  private void cd(String path) {
    lastPath = path;
    file = new File(path);
  }

  private void dir() {
    Arrays.asList(file.listFiles()).stream()
        .forEach(System.out::println);
  }
}
