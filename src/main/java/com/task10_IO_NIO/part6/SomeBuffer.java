package com.task10_IO_NIO.part6;

import java.io.IOException;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class SomeBuffer {

  public static void readSome(Path path,int bufferSize) {
    try ( SeekableByteChannel channel =  Files.newByteChannel(path, StandardOpenOption.READ)) {
      ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
      int a = channel.read((buffer));
      StringBuilder data = new StringBuilder();
      if (a != -1) {
        buffer.rewind();

        for (int i = 0; i < a; i++) {
          data.append((char) buffer.get());
        }
      }
      System.out.println(data.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void writeSome(Path path, String data) {
    try (FileChannel channel = (FileChannel) Files
        .newByteChannel(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
      ByteBuffer buffer = ByteBuffer.wrap(data.getBytes());
      channel.write(buffer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
