# Task #
1. Create Ship with Droids. Serialize and deserialize them. Use transient.
2. Compare reading and writing performance of usual and buffered reader for 200 MB file. Compare performance of buffered reader with different buffer size (e.g. 10 different size).
3. Write your implementation of InputStream with capability of push read data back to the stream.
4. Write a program that reads a Java source-code file (you provide the file name on the command line) and displays all the comments. Do not use regular expression.
5. Write a program that displays the contents of a specific directory (file and folder names + their attributes) with the possibility of setting the current directory (similar to “dir” and “cd” command line commands).
6. Try to create SomeBuffer class, which can be used for read and write data from/to channel (Java NIO)
7. Write client-server program using NIO (+ if you want, other implementation using IO). E.g. you have one server and multiple clients. A client can send direct messages to other client.

